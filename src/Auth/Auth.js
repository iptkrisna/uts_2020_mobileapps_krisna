import React, {Component} from 'react';
import {
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  Text,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
Icon.loadFont();

const width = Dimensions.get('window').width;
const facebookIcon = <Icon name="facebook-square" color="white" size={15} />;
const googleIcon = <Icon name="google" color="white" size={15} />;

export default class AuthScreen extends Component {
  static navigationOptions = {
    headerShown: false,
  };
  _facebook = () => {
    // this.props.navigation.navigate('FacebookAuth')
  };
  _google = () => {
    // this.props.navigation.navigate('GoogleAuth')
  };
  _email = () => {
    this.props.navigation.navigate('Login');
  };
  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', backgroundColor: 'white'}}>
        <Image
          style={{width: width, height: 200, backgroundColor: 'blue'}}
          source={require('../img/sayur.png')}
        />
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            padding: 20,
            marginVertical: 10,
            width: (width * 4) / 5,
          }}>
          <Text style={{textAlign: 'center', fontWeight: '500'}}>
            Login untuk mempermudah anda melacak dan memesan sayur anda
          </Text>
        </View>
        <TouchableOpacity
          style={styles.buttonAuth}
          onPress={() => this._facebook()}>
          <Text style={{color: 'white', textAlign: 'center'}}>
            {facebookIcon} Facebook
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttonAuth}
          onPress={() => this._google()}>
          <Text style={{color: 'white', textAlign: 'center'}}>
            {googleIcon} Google
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttonAuth}
          onPress={() => this._email()}>
          <Text style={{color: 'white', textAlign: 'center'}}>
            Masuk dengan email
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonAuth: {
    backgroundColor: 'green',
    padding: 10,
    width: (width * 4) / 5,
    borderRadius: 5,
    marginVertical: 10,
  },
});
